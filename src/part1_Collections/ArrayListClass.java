package part1_Collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ArrayListClass {
    private List<Person> persons = new ArrayList<Person>();

    public ArrayListClass() {
        persons = new ArrayList<Person>();
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void createPerson(String name, int age, String job, double salary) {
        Person person = new Person(name, age, job, salary);
        persons.add(person);
    }

    public void updatePerson(String name, int age, String job, double salary) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                p.setAge(age);
                p.setJob(job);
                p.setSalary(salary);
            }
        }
    }

    public void deletePerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                persons.remove(p);
                break;
            }
        }
    }

    public Person readPerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public void writePersons() {
        for (Person p : persons) {
            System.out.println(p.getName() + " is " + p.getAge() + " years old, works as " + p.getJob() + " and has the salary " + p.getSalary());
        }
    }

    public void sortPersons() {
        Collections.sort(persons, new NameComparator());
    }

    private class NameComparator implements Comparator<Person> {
        @Override
        public int compare(Person o1, Person o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    public static void main(String args[]) {
        System.out.println("ARRAY LIST");
        System.out.println("__________");

        ArrayListClass arrayListClass = new ArrayListClass();

        // CREATE
        arrayListClass.createPerson("Isabel", 27, "Graphic Designer", 43000);
        arrayListClass.createPerson("Alexander", 26, "Video Editor", 23000);
        arrayListClass.createPerson("Vladimir", 29, "Web developer", 65000);

        // WRITE ALL
        arrayListClass.writePersons();
        System.out.println();

        // READ
//        Person readPerson = arrayListClass.readPerson("Vladimir");
        Person readPerson = arrayListClass.readPerson("Vlad");
        if (readPerson != null) {
            System.out.println("Age: " + readPerson.getAge() + "| Job: " + readPerson.getJob() + "| Salary: " + readPerson.getSalary());
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // UPDATE
        String personToUpdate = "Vladimir";
        if (arrayListClass.readPerson(personToUpdate) != null) {
            arrayListClass.updatePerson(personToUpdate, 30, "Software Programmer", 67000);
            System.out.println("Updated successfully.");
//            arrayListClass.writePersons();
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // DELETE
        arrayListClass.deletePerson("Vladimir");
        arrayListClass.writePersons();
        System.out.println();

        // SORT
        arrayListClass.sortPersons();
        arrayListClass.writePersons();
        System.out.println();
    }
}
