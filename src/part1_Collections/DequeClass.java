package part1_Collections;

import java.util.Deque;
import java.util.LinkedList;

public class DequeClass {
    private Deque<Person> persons = new LinkedList<Person>();

    public DequeClass() {
        persons = new LinkedList<Person>();
    }

    public void setPersons(Deque<Person> persons) {
        this.persons = persons;
    }

    public Deque<Person> getPersons() {
        return persons;
    }

    public void createPerson(String name, int age, String job, double salary) {
        Person person = new Person(name, age, job, salary);
        persons.add(person);
    }

    public void updatePerson(String name, int age, String job, double salary) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                p.setAge(age);
                p.setJob(job);
                p.setSalary(salary);
                break;
            }
        }
    }

    public void deletePerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                persons.remove(p);
                break;
            }
        }
    }

    public void deleteFirst() {
        persons.removeFirst();
    }

    public void deleteLast() {
        persons.removeLast();
    }

    public Person readPerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public void writePersons() {
        for (Person p : persons) {
            System.out.println(p.getName() + " is " + p.getAge() + " years old, works as " + p.getJob() + " and has the salary " + p.getSalary());
        }
    }

    public static int minIndex(Deque<Person> q, int sortIndex) {
        int min_index = -1;
        String min_value = "ZZZZZZZZZZZZ";
        int size = q.size();
        for (int i = 0; i < size; i++)
        {
            Person current = q.peek();

            q.poll();

            if (current.getName().compareTo(min_value) <= 0 && i <= sortIndex)
            {
                min_index = i;
                min_value = current.getName();
            }
            q.add(current);
        }
        return min_index;
    }

    public static void insertMinToRear(Deque<Person> q, int min_index) {
        Person min_value = new Person();
        int size = q.size();
        for (int i = 0; i < size; i++)
        {
            Person current = q.peek();
            q.poll();
            if (i != min_index)
                q.add(current);
            else
                min_value = current;
        }
        q.add(min_value);
    }

    public void sortPersons() {
        for(int i = 1; i <= persons.size(); i++) {
            int min_index = minIndex(persons,persons.size() - i);
            insertMinToRear(persons, min_index);
        }
    }

    public static void main(String args[]) {
        System.out.println("DEQUE");
        System.out.println("_____");

        DequeClass dequeueClass = new DequeClass();

        // CREATE
        dequeueClass.createPerson("Olivia", 27, "Graphic Designer", 43000);
        dequeueClass.createPerson("Pavel", 24, "Personal Trainer", 20500);
        dequeueClass.createPerson("Alexander", 26, "Video Editor", 23000);
        dequeueClass.createPerson("Dimitri", 27, "Accountant", 53000);
        dequeueClass.createPerson("Paul", 22, "Personal Trainer", 20000);
        dequeueClass.createPerson("Vladimir", 29, "Web developer", 65000);

        // WRITE ALL
        dequeueClass.writePersons();
        System.out.println();

        // READ
        Person readPerson = dequeueClass.readPerson("Olivia");
        if (readPerson != null) {
            System.out.println("Age: " + readPerson.getAge() + "| Job: " + readPerson.getJob() + "| Salary: " + readPerson.getSalary());
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // UPDATE
        String personToUpdate = "Alexander";
        if (dequeueClass.readPerson(personToUpdate) != null) {
            dequeueClass.updatePerson(personToUpdate, 30, "Software Programmer", 67000);
            System.out.println("Updated successfully.");
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // DELETE
        dequeueClass.deletePerson("Vladimir");
        dequeueClass.writePersons();
        System.out.println();

        // DELETE FIRST & DELETE LAST
        dequeueClass.deleteFirst();
        dequeueClass.deleteLast();
        dequeueClass.writePersons();
        System.out.println();

        // SORT
        dequeueClass.sortPersons();
        dequeueClass.writePersons();
        System.out.println();
    }
}
