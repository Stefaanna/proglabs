package part1_Collections;

import java.util.*;

public class LinkedListClass {
    private LinkedList<Person> persons = new LinkedList<Person>();

    public LinkedListClass() {
        persons = new LinkedList<Person>();
    }

    public void setPersons(LinkedList<Person> persons) {
        this.persons = persons;
    }

    public LinkedList<Person> getPersons() {
        return persons;
    }

    public void createPerson(String name, int age, String job, double salary) {
        Person person = new Person(name, age, job, salary);
        persons.add(person);
    }

    public void updatePerson(String name, int age, String job, double salary) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                p.setAge(age);
                p.setJob(job);
                p.setSalary(salary);
                break;
            }
        }
    }

    public void deletePerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                persons.remove(p);
                break;
            }
        }
    }

    public Person readPerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public void writePersons() {
        for (Person p : persons) {
            System.out.println(p.getName() + " is " + p.getAge() + " years old, works as " + p.getJob() + " and has the salary " + p.getSalary());
        }
    }

    public void sortPersons() {
        Collections.sort(persons, new NameComparator());
    }

    private class NameComparator implements Comparator<Person> {
        @Override
        public int compare(Person o1, Person o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    public static void main(String args[]) {
        System.out.println("LINKED LIST");
        System.out.println("___________");

        LinkedListClass linkedListClass = new LinkedListClass();

        // CREATE
        linkedListClass.createPerson("Olivia", 27, "Graphic Designer", 43000);
        linkedListClass.createPerson("Alexander", 26, "Video Editor", 23000);
        linkedListClass.createPerson("Vladimir", 29, "Web developer", 65000);

        // WRITE ALL
        linkedListClass.writePersons();
        System.out.println();

        // READ
        Person readPerson = linkedListClass.readPerson("Olivia");
        if (readPerson != null) {
            System.out.println("Age: " + readPerson.getAge() + "| Job: " + readPerson.getJob() + "| Salary: " + readPerson.getSalary());
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // UPDATE
        String personToUpdate = "Alexander";
        if (linkedListClass.readPerson(personToUpdate) != null) {
            linkedListClass.updatePerson(personToUpdate, 30, "Software Programmer", 67000);
            System.out.println("Updated successfully.");
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // DELETE
        linkedListClass.deletePerson("Vladimir");
        linkedListClass.writePersons();
        System.out.println();

        // SORT
        linkedListClass.sortPersons();
        linkedListClass.writePersons();
        System.out.println();
    }
}
