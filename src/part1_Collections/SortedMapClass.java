package part1_Collections;

import java.util.*;

public class SortedMapClass {
    private SortedMap<Integer, Person> persons = new TreeMap<Integer, Person>();

    public SortedMapClass() {
        persons = new TreeMap<Integer, Person>();
    }

    public void setPersons(SortedMap<Integer, Person> persons) {
        this.persons = persons;
    }

    public SortedMap<Integer, Person> getPersons() {
        return persons;
    }

    public void createPerson(Integer key, String name, int age, String job, double salary) {
        Person person = new Person(name, age, job, salary);
        persons.put(key, person);
    }

    public void updatePerson(String name, int age, String job, double salary) {
        Set set = persons.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Person p = (Person) entry.getValue();
            if (p.getName().equals(name)) {
                p.setAge(age);
                p.setJob(job);
                p.setSalary(salary);
                break;
            }
        }
    }

    public void deletePerson(Integer key) {
        persons.remove(key);
    }

    public Person readPerson(String name) {
        Set set = persons.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Person p = (Person) entry.getValue();
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public void writePersons() {
        /*
        Set set = persons.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Person p = (Person) entry.getValue();
            System.out.println(p.getName() + " is " + p.getAge() + " years old, works as " + p.getJob() + " and has the salary " + p.getSalary());
        }
        */
        for (Map.Entry<Integer, Person> entry : persons.entrySet()) {
            System.out.println(entry.getValue().getName() + " is " + entry.getValue().getAge() + " years old, works as " + entry.getValue().getJob() + " and has the salary " + entry.getValue().getSalary());

        }
    }

    public void sortPersons() {
        // create a list from the map elements
        List<Map.Entry<Integer, Person>> list = new LinkedList<Map.Entry<Integer, Person>>(persons.entrySet());

        // sorting the list
        Collections.sort(list, new NameComparator());

        // put sorted data into map
        SortedMap<Integer, Person> sortedMap = new TreeMap<Integer, Person>();
        for (Map.Entry<Integer, Person> p : list) {
            System.out.println(p.getValue().getName() + " is " + p.getValue().getAge() + " years old, works as " + p.getValue().getJob() + " and has the salary " + p.getValue().getSalary());
            sortedMap.put(p.getKey(), p.getValue());
        }
        persons.clear();
        persons.putAll(sortedMap);
    }

    private class NameComparator implements Comparator<Map.Entry<Integer, Person>> {
        @Override
        public int compare(Map.Entry<Integer, Person> o1, Map.Entry<Integer, Person> o2) {
            return (o1.getValue().getName().compareTo(o2.getValue().getName()));
        }
    };


    public static void main(String args[]) {
        System.out.println("SORTED MAP");
        System.out.println("__________");

        SortedMapClass sortedMapClass = new SortedMapClass();

        // CREATE
        sortedMapClass.createPerson(1, "Isabel", 27, "Graphic Designer", 43000);
        sortedMapClass.createPerson(2, "Vladimir", 26, "Lawyer", 59000);
        sortedMapClass.createPerson(3, "Dimitri", 28, "Accountant", 53400);
        sortedMapClass.createPerson(4, "Alexander", 26, "Video Editor", 23000);
        sortedMapClass.createPerson(5, "Olivia", 29, "Web developer", 65000);

        // WRITE ALL
        sortedMapClass.writePersons();
        System.out.println();

        // READ
//        Person readPerson = arrayListClass.readPerson("Vladimir");
        Person readPerson = sortedMapClass.readPerson("Olivia");
        if (readPerson != null) {
            System.out.println("Age: " + readPerson.getAge() + "| Job: " + readPerson.getJob() + "| Salary: " + readPerson.getSalary());
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // UPDATE
        String personToUpdate = "Vladimir";
        if (sortedMapClass.readPerson(personToUpdate) != null) {
            sortedMapClass.updatePerson(personToUpdate, 30, "Software Programmer", 67000);
            System.out.println("Updated successfully.");
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // DELETE
        sortedMapClass.deletePerson(1);
        sortedMapClass.writePersons();
        System.out.println();

        // SORT
        sortedMapClass.sortPersons();
        System.out.println();
    }
}
