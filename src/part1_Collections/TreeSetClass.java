package part1_Collections;

import java.util.*;

public class TreeSetClass {
    private TreeSet<Person> persons = new TreeSet<Person>();

    public TreeSetClass() {
        persons = new TreeSet<Person>();
    }

    public void setPersons(TreeSet<Person> persons) {
        this.persons = persons;
    }

    public TreeSet<Person> getPersons() {
        return persons;
    }

    public void createPerson(String name, int age, String job, double salary) {
        Person person = new Person(name, age, job, salary);
        persons.add(person);
    }

    public void updatePerson(String name, int age, String job, double salary) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                p.setAge(age);
                p.setJob(job);
                p.setSalary(salary);
            }
        }
    }

    public void deletePerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                persons.remove(p);
                break;
            }
        }
    }

    public Person readPerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public void writePersons() {
        for (Person p : persons) {
            System.out.println(p.getName() + " is " + p.getAge() + " years old, works as " + p.getJob() + " and has the salary " + p.getSalary());
        }
    }

    public void sortPersons() {
        List<Person> list = new ArrayList<Person>(persons);
        Collections.sort(list, new NameComparator());
        for (Person p : list) {
            System.out.println(p.getName() + " is " + p.getAge() + " years old, works as " + p.getJob() + " and has the salary " + p.getSalary());
        }
    }

    private class NameComparator implements Comparator<Person> {
        @Override
        public int compare(Person o1, Person o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    public static void main(String args[]) {
        System.out.println("TREE SET");
        System.out.println("_________");

        TreeSetClass treeSetClass = new TreeSetClass();

        // CREATE
        treeSetClass.createPerson("Olivia", 27, "Graphic Designer", 43000);
        treeSetClass.createPerson("Alexander", 26, "Video Editor", 23000);
        treeSetClass.createPerson("Vladimir", 29, "Web developer", 65000);
        treeSetClass.createPerson("Sam", 25, "Lawyer", 60000);

        // WRITE ALL
        treeSetClass.writePersons();
        System.out.println();

        // READ
        Person readPerson = treeSetClass.readPerson("Olivia");
        if (readPerson != null) {
            System.out.println("Age: " + readPerson.getAge() + "| Job: " + readPerson.getJob() + "| Salary: " + readPerson.getSalary());
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // UPDATE
        String personToUpdate = "Vladimir";
        if (treeSetClass.readPerson(personToUpdate) != null) {
            treeSetClass.updatePerson(personToUpdate, 30, "Software Programmer", 67000);
            System.out.println("Updated successfully.");
        } else {
            System.out.println("The person does not exist.");
        }
        System.out.println();

        // DELETE
        treeSetClass.deletePerson("Olivia");
        treeSetClass.writePersons();
        System.out.println();

        // SORT
        treeSetClass.sortPersons();
        System.out.println();
    }
}
