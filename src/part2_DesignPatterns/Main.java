package part2_DesignPatterns;

import part2_DesignPatterns.BuilderPattern.BuilderExample;
import part2_DesignPatterns.DecoratorPattern.DecoratorExample;
import part2_DesignPatterns.FacadePattern.Client;
import part2_DesignPatterns.IteratorPattern.IteratorExample;
import part2_DesignPatterns.ObserverPattern.ObserverExample;
import part2_DesignPatterns.PrototypePattern.PrototypeDemo;
import part2_DesignPatterns.SingletonPattern.SingletonExample;
import part2_DesignPatterns.StrategyPattern.StrategyExample;

public class Main {
    public static void main(String args[]) {
        // --------------------------------------
        // Examples of Creational Design Patterns
        // --------------------------------------

        // Singleton
        System.out.println("\nSingleton");
        SingletonExample singletonExample = SingletonExample.getInstance();
        singletonExample.getInfo();

        // Builder
        System.out.println("\nBuilder");
        BuilderExample builderExample = new BuilderExample();
        builderExample.demo();

        // Prototype
        System.out.println("\nPrototype");
        PrototypeDemo prototypeDemo = new PrototypeDemo();
        prototypeDemo.demo();

        // --------------------------------------
        // Examples of Structural Design Patterns
        // --------------------------------------

        // Facade
        System.out.println("\nFacade");
        Client client = new Client();
        client.demo();

        // Decorator
        System.out.println("\nDecorator");
        DecoratorExample decoratorExample = new DecoratorExample();
        decoratorExample.demo();

        // --------------------------------------
        // Example of Behavioral Design Patterns
        // --------------------------------------

        // Iterator
        System.out.println("\nIterator");
        IteratorExample iteratorExample = new IteratorExample();
        iteratorExample.demo();

        // Observer
        System.out.println("\nObserver");
        ObserverExample observerExample = new ObserverExample();
        observerExample.demo();

        // Strategy
        System.out.println("\nStrategy");
        StrategyExample strategyExample = new StrategyExample();
        strategyExample.demo();
    }
}
