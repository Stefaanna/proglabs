package part2_DesignPatterns.BuilderPattern;

public class Person {
    private String name;
    private int age;
    private String email;

    public Person(String name, int age, String email) {
        this.name = name;
        this.age = age;
        this.email = email;
    }

    public static class Builder {
        private String name;
        private int age;
        private String email;

        public Builder() {}

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setAge(int age) {
            this.age = age;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Person build() {
            return new Person(name, age, email);
        }
    }

    public void getInfo() {
        System.out.println(name + " has " + age + " years");
    }
}
