package part2_DesignPatterns.BuilderPattern;

public class BuilderExample {
    public BuilderExample() {}

    public void demo() {
        Person person1 = new Person.Builder()
                .setName("Random P")
                .setAge(100)
                .setEmail("email@example.com")
                .build();
        person1.getInfo();
        Person person2 = new Person.Builder()
                .setName("Random P")
                .setEmail("email@example.com")
                .build();
        person2.getInfo();
    }
}
