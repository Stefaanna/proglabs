package part2_DesignPatterns.ObserverPattern;

public class ObserverExample {
    public ObserverExample() {}

    public void demo() {
        Subject subject = new Subject();

        new HexaObserver(subject);
        new OctalObserver(subject);
        new BinaryObserver(subject);

        System.out.println("Integer: 10");
        subject.setState(10);
    }
}
