package part2_DesignPatterns.ObserverPattern;

public abstract class Observer {
    public Subject subject;

    public void update() {}
}
