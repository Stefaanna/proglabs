package part2_DesignPatterns.PrototypePattern;

public class PrototypeDemo {
    public PrototypeDemo() {}

    public void demo() {
        Prototype.getFigure("circle").addFigure();
        Prototype.getFigure("square").addFigure();
        Prototype.getFigure("circle").addFigure();
    }
}
