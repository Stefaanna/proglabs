package part2_DesignPatterns.PrototypePattern;

public class Circle extends Figure {
    public Circle() {
        this.name = "Circle";
    }

    @Override
    public void addFigure() {
        System.out.println("Circle added");
    }
}
