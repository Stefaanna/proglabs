package part2_DesignPatterns.PrototypePattern;

public class Square extends Figure {
    public Square() {
        this.name = "Square";
    }

    @Override
    public void addFigure() {
        System.out.println("Square added");
    }
}
