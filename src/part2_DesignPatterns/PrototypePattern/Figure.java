package part2_DesignPatterns.PrototypePattern;

public abstract class Figure implements Cloneable {
    protected String name;

    public abstract void addFigure();

    public Object clone() {
        Object clone = null;
        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}
