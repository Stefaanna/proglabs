package part2_DesignPatterns.PrototypePattern;

import java.util.HashMap;
import java.util.Map;

public class Prototype {
    private static Map<String, Figure> figures = new HashMap<String, Figure>();

    static
    {
        figures.put("circle", new Circle());
        figures.put("square", new Square());
    }

    public static Figure getFigure(String figureName) {
        return (Figure) figures.get(figureName).clone();
    }
}
