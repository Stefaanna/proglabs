package part2_DesignPatterns.StrategyPattern;

public class StrategyExample {
    public StrategyExample() {}

    public void demo() {
        Context context = new Context(new OperationAdd());
        System.out.println("6 + 7 = " + context.executeStrategy(6, 7));

        context = new Context(new OperationSubstract());
        System.out.println("6 - 7 = " + context.executeStrategy(6, 7));

        context = new Context(new OperationMultiply());
        System.out.println("6 * 7 = " + context.executeStrategy(6, 7));
    }
}
