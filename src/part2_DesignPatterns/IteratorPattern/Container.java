package part2_DesignPatterns.IteratorPattern;

public interface Container {
    public Iterator getIterator();
}
