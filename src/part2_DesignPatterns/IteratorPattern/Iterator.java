package part2_DesignPatterns.IteratorPattern;

public interface Iterator {
    public boolean hasNext();
    public Object next();
}
