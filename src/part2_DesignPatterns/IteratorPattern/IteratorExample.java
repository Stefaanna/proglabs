package part2_DesignPatterns.IteratorPattern;

public class IteratorExample {
    public IteratorExample() {}

    public void demo() {
        NameContainer namesRepository = new NameContainer();

        for(Iterator iter = namesRepository.getIterator(); iter.hasNext();){
            String name = (String)iter.next();
            System.out.println("Name : " + name);
        }
    }
}
