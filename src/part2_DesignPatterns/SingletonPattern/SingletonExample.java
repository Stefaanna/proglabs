package part2_DesignPatterns.SingletonPattern;

public class SingletonExample {
    private static SingletonExample instance;

    private SingletonExample() {}

    public static SingletonExample getInstance() {
        if (instance == null) {
            instance = new SingletonExample();
        }
        return instance;
    }

    public void getInfo() {
        System.out.println("This is a singleton pattern example.");
    }
}
