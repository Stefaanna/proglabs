package part2_DesignPatterns.DecoratorPattern;

public class DecoratorExample {
    public DecoratorExample() {}

    public void demo() {
        Shape circle = new Circle();
        Shape redCircle = new RedShapeDecorator(new Circle());
        Shape redSquare = new RedShapeDecorator(new Square());

        System.out.println("Circle with normal border");
        circle.draw();

        System.out.println("\nCircle with red border");
        redCircle.draw();

        System.out.println("\nSquare with red border");
        redSquare.draw();
    }
}
