package part2_DesignPatterns.DecoratorPattern;

public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("Shape: Square");
    }
}
