package part2_DesignPatterns.DecoratorPattern;

public interface Shape {
    void draw();
}
