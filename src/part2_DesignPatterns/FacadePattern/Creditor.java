package part2_DesignPatterns.FacadePattern;

public class Creditor {
    public void creditAccount(String accNo, Double amount) {}
}
