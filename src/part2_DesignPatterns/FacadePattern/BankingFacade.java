package part2_DesignPatterns.FacadePattern;

public class BankingFacade {
    Creditor creditor = new Creditor();
    Debitor debitor = new Debitor();
    FDIssuer fdIssuer = new FDIssuer();

    public void onlineTransfer(String fromAcc, String toAcc, Double amount) {
        creditor.creditAccount(toAcc, amount);
        debitor.debitAccount(fromAcc, amount);
    }

    public void atmWithdrawal(String fromAcc, Double amount) {
        debitor.debitAccount(fromAcc, amount);
    }

    public void issueFd(String fromAcc, Double amount) {
        debitor.debitAccount(fromAcc, amount);
        fdIssuer.issueFD(amount);
    }
}
