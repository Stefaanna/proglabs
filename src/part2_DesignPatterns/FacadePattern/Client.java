package part2_DesignPatterns.FacadePattern;

public class Client {
    private BankingFacade facade;

    public Client() {

    }

    public Client(BankingFacade facade) {
        this.facade = facade;
    }

    public void demo() {
        System.out.println("This is a facade pattern example.");
        BankingFacade facade = new BankingFacade();
        facade.onlineTransfer("ROTYD356745", "ROBTY356478", 1700.00);
        facade.atmWithdrawal("ROTYD356745", 140.00);
        facade.issueFd("ROTYD356745", 755.00);
    }
}
